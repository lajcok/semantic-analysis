import {Extractor, IExtractionResult} from './extraction';
import {IWebPage, loadPage} from '../tools/WebPage';
import DocumentHandler from '../tools/DocumentHandler';
import Validator from './validation';

const analysis = async (url: string, source?: string): Promise<IExtractionResult> =>
    new Promise<IExtractionResult>((resolve, reject) =>
        (!source
                ? loadPage(url)
                : new Promise<IWebPage>(res => res({url, body: source}))
        )
            .then(page => {
                const doc = new DocumentHandler(page.body, page.url);
                const res = new Extractor(doc).extract();
                new Validator().validate(res);
                resolve(res);
            })
            .catch(reject)
    );

export default analysis;
