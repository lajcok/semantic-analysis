export * from './ISchema';
export * from './ITerm';
export * from './util';

import {SchemaProvider} from './SchemaProvider';
export default SchemaProvider;
