export type TermType = 'Class' | 'Property' | IClass;

/**
 * Universal term structure
 */
export interface ITerm {

    /**
     * The `@id` of the term
     */
    id: string;

    /**
     * The `@type`s of the term
     *
     * - `'Class'` -- `rdfs:Class`,
     * - `'Property'` -- `rdf:Property`,
     * - or a reference to another class.
     */
    type?: TermType[];

    /**
     * A `rdfs:label` for the term
     */
    label?: string;

    /**
     * A `rdfs:comment` for the term
     */
    comment?: string

    /**
     * An alternative term(s) for deprecated ones
     * @see https://meta.schema.org/supersededBy
     */
    supersededBy?: ITerm[]

}

/**
 * A class or type
 * Specific case of a term
 * @see https://www.w3.org/2000/01/rdf-schema#Class
 */
export interface IClass extends ITerm {

    /**
     * The term is subclass of these
     */
    subClassOf?: IClass[];

}

/**
 * A property on some class(es)
 * Specific case of a term
 * @see http://www.w3.org/1999/02/22-rdf-syntax-ns#Property
 */
export interface IProperty extends ITerm {

    /**
     * Possible value types for the property
     */
    rangeIncludes?: IClass[]

    /**
     * Property appears on these classes
     */
    domainIncludes?: IClass[]

}
