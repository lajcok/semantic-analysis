import {IExtractionResult, IExtractor} from "./IExtractor";
import {ExtractorContainer} from "./ExtractorContainer";
import {MicrodataExtractor} from "./MicrodataExtractor";
import {JsonExtractor} from "./JsonExtractor";
import DocumentHandler from '../../tools/DocumentHandler';

/**
 * Master extractor using common sub-extractors
 */
export class Extractor implements IExtractor {

    /* Properties */

    private readonly container: ExtractorContainer;

    /* Initialization */

    constructor(private readonly doc: DocumentHandler) {
        this.container = new ExtractorContainer(
            new MicrodataExtractor(doc),
            new JsonExtractor(doc),
        );
    }

    /* Extractor */

    public extract = (): Readonly<IExtractionResult> => ({
        ...this.container.extract(),
        document: this.doc,
    });

}
