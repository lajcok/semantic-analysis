import DocumentHandler from '../../tools/DocumentHandler';
import {Extractor} from '../extraction';
import {toSerializable} from './serializable';
import Validator from '../validation';

test('Serializable', () => {
    const source = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Jiří Lajčok</title>
</head>
<body>

<section itemscope itemtype="http://schema.org/Person">
    <link itemprop="url" href="http://lajcok.cz">
    <h3 itemprop="name">Jiří Lajčok</h3>
    <p>
        Contact me at
        <a href="mailto:jiri@lajcok.cz" itemprop="email">
            jiri@lajcok.cz
        </a>
    </p>
</section>

</body>
</html>
`;
    const doc = new DocumentHandler(source);
    const res = new Extractor(doc).extract();
    new Validator().validate(res);
    const serializable = toSerializable(res);

    expect(() => {
        const json = JSON.stringify(serializable);
        console.debug(json);
    }).not.toThrow();
});
