import {IExtractionResult, IItem, ILog, IMeta} from '../../';
import {ITerm} from '../validation/schema';
import {isItem} from './spec';
import {map} from '../../util';

/* Structure */

export interface IResultSimple {
    items: readonly IItemSimple[];
    document: {url?: string};
    logs: readonly ILogSimple[];
}

export interface IItemSimple {
    type: readonly string[];
    id?: string;
    meta: IMetaSimple;
    properties: { [name: string]: readonly IPropertySimple[] }
}

export interface IPropertySimple {
    value: ValueSimple;
    meta: IMetaSimple;
}

export type ValueSimple = IItemSimple | string;

export interface IMetaSimple {
    type: 'JSON-LD' | 'Microdata' | 'RDFa' | 'Microformats';
    logs: readonly ILogSimple[];
}

export interface ILogSimple {
    sort: 'Notice' | 'Warning' | 'Error';
    message: string;
    phase: 'Parsing' | 'Extraction' | 'Semantics';
    term?: ITerm;
}

/* Simplification */

export const toSerializable = ({items, document, logs}: IExtractionResult): IResultSimple => ({
    items: items.map(simplifyItem),
    document: {url: document?.url},
    logs: logs.map(simplifyLog),
});

const simplifyItem = ({type, id, meta, propMeta}: IItem): IItemSimple => ({
    type,
    id,
    meta: simplifyMeta(meta),
    properties: map(propMeta, (props =>
        props.map(({meta, value}) => ({
            meta: simplifyMeta(meta),
            value: isItem(value) ? simplifyItem(value) : value,
        }))
    )),
});

const simplifyMeta = ({type, logs}: IMeta) => ({
    type,
    logs: logs.map(simplifyLog),
});

const simplifyLog =
    ({sort, message, phase, term}: ILog): ILogSimple =>
        ({sort, message, phase, term});
