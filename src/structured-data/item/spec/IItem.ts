/**
 * Property value type, extendable by generics
 */
export type Value<T extends IItem = IItem> = T | string;

/**
 * A list of name-value pairs
 */
export interface IProperties<T extends IItem = IItem> {
    [name: string]: Value[];
}

/**
 * Interface representing an item
 * @see https://html.spec.whatwg.org/multipage/microdata.html
 */
export interface IItem {

    /**
     * The type of the item
     */
    type: string[];

    /**
     * The ID of the item
     */
    id?: string;

    /**
     * Properties of the item
     */
    properties: IProperties;

}