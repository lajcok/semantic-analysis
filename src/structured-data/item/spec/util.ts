import {IItem, Value} from "./IItem";

/**
 * Item interface type guard
 * @param value To inspect
 */
export const isItem =
    <T extends IItem = IItem>(value: Value<T>): value is T =>
        !isValuePrimitive(value);

/**
 * String type guard
 * @param value To inspect
 */
export const isValuePrimitive =
    (value: Value): value is string =>
        typeof value === 'string';

/**
 * Find out whether is valid URL
 * @param s To check
 */
export function isUrl(s: string) {
    try {
        new URL(s).toString();
        return true;
    } catch (e) {
        return false;
    }
}

/**
 * Combines term to the base address
 * @param term Term to complete
 * @param base Base to apply term to
 */
export function combine(term: string, base?: string) {
    try {
        if (!base || isUrl(term)) {
            return term;
        }

        const b = new URL(base);
        if (b.hash.length > 0 || b.toString().endsWith('#')) {
            b.hash = term;
            return b.toString();
        }

        return new URL(term, base).toString();
    } catch (e) {
        return term;
    }
}
