import * as Spec from './spec';

export * from "./IItem";
export * from "./IMeta";
export * from "./ILog";
export * from './serializable';
export {Spec};
