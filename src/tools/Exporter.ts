import {IExtractionResult} from '../structured-data/extraction';
import fs from 'fs';
import path from 'path';
import {IResultSimple, toSerializable} from '../structured-data/item';

export interface IExportable {
    results: IResultSimple;
    url?: string;
}

export interface IExportInfo {
    json: string;
    html: string;
}

export interface IExported extends IExportable {
    paths: IExportInfo;
}

export interface IExportFailed extends IExported {
    error: any;
}

export const prepareExport = (result: IExtractionResult): IExportable => ({
    results: toSerializable(result),
    url: result.document?.url,
});

export default class Exporter {

    /* Properties */

    public readonly extractionDirectory: string;

    /* Initialization */

    constructor(extractionDirectory: string) {
        this.extractionDirectory = path.resolve(extractionDirectory);
        if (!fs.existsSync(this.extractionDirectory)) {
            console.debug(`Creating directory ${this.extractionDirectory}`);
            fs.mkdirSync(this.extractionDirectory);
        }
    }

    /* API */

    async export(results: IExtractionResult): Promise<IExported> {
        const timestamp = Date.now();
        let safeUrl: string;
        try {
            const url = new URL(results.document!.url!);
            const urlFormat = `${url.host}/${url.pathname}${url.search}`;
            safeUrl = urlFormat.replace(/[^a-z0-9]/gi, '_').toLowerCase();
        } catch {
            safeUrl = '';
        }
        const base = `${timestamp}_${safeUrl}`;

        const source = results.document!.source;
        const json = `${base}.json`;
        const html = `${base}.html`;

        const jsonWriting = fs.promises.writeFile(
            path.join(this.extractionDirectory, json),
            JSON.stringify(prepareExport(results)),
            'utf8',
        );
        const htmlWriting: Promise<void> = source ?
            fs.promises.writeFile(
                path.join(this.extractionDirectory, html),
                source,
                'utf8',
            ) : Promise.resolve();

        const exported: IExported = {
            ...prepareExport(results),
            paths: {json, html},
        };

        return Promise.all([jsonWriting, htmlWriting])
            .then(() => exported)
            .catch(reason => {
                throw ({
                    error: reason,
                    ...exported
                } as IExportFailed);
            });
    }

}