import * as fs from "fs";
import * as readline from "readline";
import {VisitLog} from './VisitLog';

/**
 * Logs visited URLs to prevent repeated scans
 * with file backing up solution
 */
export class VisitLogFile extends VisitLog {

    /* Properties */

    private logger?: fs.WriteStream;

    /* Initialization */

    constructor(private readonly path?: string) {
        super();
    }

    readonly setupFromFile = async (path?: string) => new Promise<boolean>(resolve => {
        const rlPath = path ?? this.path;
        if (rlPath && fs.existsSync(rlPath) && fs.lstatSync(rlPath).isFile()) {
            const readIf = readline.createInterface({input: fs.createReadStream(rlPath)});
            readIf.on('line', line => this.add(line));
            readIf.on('close', () => resolve(true));
        } else {
            resolve(false);
        }
    });

    /* Manipulation */

    add(url: string) {
        super.add(url);
        if (!this.logger && this.path) {
            this.logger = fs.createWriteStream(this.path, {flags: 'a'});
        }
        this.logger?.write(url + '\n');
    }

}