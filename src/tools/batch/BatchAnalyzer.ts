import analysis from '../../structured-data/analysis';
import {
    IBatchAnalyzer,
    IOptions,
    ITask,
    ITaskArchived,
    ITaskDone,
    ITaskFailed,
    ITaskQueued,
    ITaskRunning
} from './IBatchAnalyzer';

type TimeoutHandler = ReturnType<typeof setTimeout>;

/**
 * General batch analysis class
 */
export class BatchAnalyzer implements IBatchAnalyzer {

    /* Properties */

    private readonly options: IOptions;

    public readonly queued: ITaskQueued[] = [];
    public readonly running: ITaskRunning[] = [];
    public readonly done: ITaskArchived[] = [];
    public readonly failed: ITaskArchived[] = [];

    private coolingDown?: TimeoutHandler;
    private disposed = false;

    /* Initialization */

    constructor(options?: Partial<IOptions>) {
        this.options = {
            ...(options ?? {}),
            rateLimit: options?.rateLimit ?? 0,
            maxRunning: options?.maxRunning ?? 1,
        };
    }

    /* Interface */

    addTask(url: string, source?: string): Promise<ITaskDone> {
        if (this.disposed) {
            throw 'The analyzer has already been disposed, get a new one.';
        }
        return new Promise<ITaskDone>((resolve, reject) => {
            const task = {
                url, source,
                resolver: {resolve, reject},
                queued: new Date(),
            };
            this.queued.push(task);
            this.taskUpdate();
            this.trigger();
        })
            .then(done => {
                try {
                    this.options.doneCallback?.(done);
                    this.options.callback?.(done);
                }
                catch (e) {
                    console.error('error in callback', e)
                }
                return done;
            })
            .catch(failed => {
                try {
                    this.options.failedCallback?.(failed);
                    this.options.callback?.(failed);
                }
                catch (e) {
                    console.error('error in callback', e)
                }
                return failed;
            });
    }

    private trigger() {
        if (this.coolingDown === undefined && this.running.length < this.options.maxRunning) {
            const task = this.queued.shift();
            if (task) {
                this.runTask(task);
            }
        }
    }

    private runTask(task: ITaskQueued) {
        const taskRunning: ITaskRunning = {
            handler: analysis(task.url, task.source),
            run: new Date(),
            ...task
        };
        this.running.push(taskRunning);
        this.taskUpdate();

        taskRunning.handler
            .then(results => {
                const taskDone: ITaskDone = {
                    results,
                    done: new Date(),
                    ...taskRunning
                };
                if (!this.options.exporter) {
                    this.done.push(archive(taskDone));
                    taskRunning.resolver.resolve(taskDone);
                    return Promise.resolve()
                } else {
                    return this.options.exporter.export(taskDone.results)
                        .then(exported => {
                            const taskExported: ITaskDone = {
                                ...taskDone,
                                exportInfo: exported.paths,
                            };
                            this.done.push(archive(taskExported));
                            taskRunning.resolver.resolve(taskExported);
                        })
                        .catch(error => {
                            console.error(`Failed to export [${taskDone.url}]`, error);
                            const taskFailed: ITaskFailed = {
                                ...taskDone,
                                failed: new Date(),
                                error: taskDone.error ? [taskDone.error, error] : error,
                            };
                            this.failed.push(archive(taskFailed));
                            taskRunning.resolver.reject(taskFailed);
                        });
                }
            })
            .catch(error => {
                console.error(`Task could not completed [${taskRunning.url}]`, error);
                const taskFailed: ITaskFailed = {
                    ...taskRunning,
                    failed: new Date(),
                    error: taskRunning.error ? [taskRunning.error, error] : error,
                };
                this.failed.push(archive(taskFailed));
                taskRunning.resolver.reject(taskFailed);
            })
            .finally(() => {
                this.running.splice(this.running.indexOf(taskRunning), 1);
                this.taskUpdate();
                this.coolDown();
            });
    }

    private coolDown() {
        this.coolingDown = this.coolingDown ?? setTimeout(() => {
            this.coolingDown = undefined;
            this.trigger();
        }, this.options.rateLimit);
    }

    readonly state = () => ({
        queued: this.queued.map(archive),
        running: this.running.map(archive),
        done: this.done,
        failed: this.failed,
    })

    readonly taskUpdate = () =>
        this.options.onTaskUpdate?.(this.state());

    /* Disposable */

    dispose() {
        if (this.coolingDown !== undefined) {
            clearTimeout(this.coolingDown);
            this.coolingDown = undefined;
        }
        this.disposed = true;
    }

}

const archive = ({url, queued, run, done, failed, error, exportInfo}: ITask): ITaskArchived =>
    ({url, queued, run, done, failed, error, exportInfo});
