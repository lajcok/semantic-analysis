import {IBatchAnalyzer, IOptions, ITaskDone} from "./IBatchAnalyzer";
import {IDocumentHandler} from "../DocumentHandler";
import {BatchAnalyzer} from "./BatchAnalyzer";
import {IVisitLog} from "./VisitLog";
import {VisitLogFile} from './VisitLogFile';

export interface ICrawlOptions extends IOptions {
    visitLog?: IVisitLog;
    visitLogPath?: string;
    perSiteLimit?: number;
}

/**
 * Analyzer crawling through on-site links
 */
export class CrawlAnalyzer implements IBatchAnalyzer {

    /* Properties */

    private readonly batchAnalyzer: BatchAnalyzer;
    private readonly visitLog: IVisitLog;
    private readonly logReady: Promise<void>;

    /* Initialization */

    constructor(private readonly options?: Partial<ICrawlOptions>) {
        this.batchAnalyzer = new BatchAnalyzer(options);
        this.visitLog = options?.visitLog ?? new VisitLogFile();
        this.logReady = options?.visitLogPath && this.visitLog instanceof VisitLogFile
            ? this.visitLog.setupFromFile(options.visitLogPath).then()
            : Promise.resolve();
    }

    /* Interface */

    public get queued() {
        return this.batchAnalyzer.queued;
    }
    public get running() {
        return this.batchAnalyzer.running;
    }
    public get done() {
        return this.batchAnalyzer.done;
    }
    public get failed() {
        return this.batchAnalyzer.failed;
    }

    readonly addTask = (url: string, source?: string): Promise<ITaskDone> =>
        this.logReady
            .then(() => {
                this.visitLog.add(url);
                return this.batchAnalyzer.addTask(url, source);
            })
            .then(done => {
            const doc = done.results.document;
            const yetToDo = ((this.options?.perSiteLimit ?? Infinity) - (this.visitLog.siteCount(url) ?? 0))

            console.debug({yetToDo})

            if (doc && yetToDo > 0) {
                getSameSiteLinks(doc)
                    .filter(url => !this.visitLog.has(url))
                    .slice(0, yetToDo)
                    .forEach(url => this.addTask(url));
            }
            return done;
        });

    readonly state = () => this.batchAnalyzer.state();
    
    readonly dispose = () => this.batchAnalyzer.dispose();

}

function getSameSiteLinks(doc: IDocumentHandler): readonly string[] {
    let url: URL;
    try {
        url = new URL(doc.url!);
    } catch (e) {
        console.error('Invalid URL observed', e);
        return [];
    }

    return Array.from(doc.document.querySelectorAll('a[href]') as NodeListOf<HTMLAnchorElement>)
        .map(({href}) => {
            try {
                return new URL(href, url);
            } catch (e) {
                console.error('Cannot create target URL', e);
                return undefined;
            }
        })
        .filter(target => target?.host === url.host)
        .map(target => target!.toString());
}
