import {createURL} from '../../util/url';

export interface IVisitLog {

    add(url: string): void;

    has(url: string): boolean;

    siteCount(url: string): number | undefined;

}

export class VisitLog implements IVisitLog {

    /* Properties */

    private readonly visited = new Set<string>();
    private readonly counts = new Map<string, number>();

    /* Visit Log */

    add(url: string) {
        this.visited.add(url);

        const host = createURL(url)?.host;
        if (host) {
            const count = this.counts.get(host) ?? 0;
            this.counts.set(host, count + 1);
        }
        else {
            console.error('Invalid URL: Host could not be extracted properly');
        }
    }

    readonly has = (url: string) => this.visited.has(url);

    readonly siteCount = (url: string) => {
        const host = createURL(url)?.host;
        return host ? this.counts.get(host) : undefined;
    }

}
