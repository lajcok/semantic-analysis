import {IExtractionResult} from "../../structured-data/extraction";
import Exporter, {IExportInfo} from '../Exporter';

export type ITaskBase = Readonly<{
    url: string;
    source?: string;
    resolver: {
        resolve: (value?: ITaskDone) => void;
        reject: (reason?: ITaskFailed) => void;
    };
    queued: Date;
    handler: Promise<IExtractionResult>;
    run: Date;
    done: Date;
    results: IExtractionResult;
    failed: Date;
    error: unknown;
    exportInfo?: IExportInfo;
}>;

export type ITask = Pick<ITaskBase, 'url' | 'source' | 'resolver' | 'queued'> & Partial<ITaskBase>;
export type ITaskQueued = ITask;
export type ITaskRunning = ITaskQueued & Pick<ITaskBase, 'handler' | 'run'>;
export type ITaskDone = ITaskRunning & Pick<ITaskBase, 'done' | 'results' | 'exportInfo'>;
export type ITaskFailed = ITaskRunning & Pick<ITaskBase, 'failed' | 'error'>;
export type ITaskArchived = Pick<ITaskBase, 'url' | 'queued'> &
    Partial<Pick<ITaskBase, 'run' | 'done' | 'failed' | 'error' | 'exportInfo'>>;

/**
 * Common options for batch analyzers
 */
export interface IOptions {
    rateLimit: number;
    maxRunning: number;
    doneCallback?: (done: ITaskDone) => void;
    failedCallback?: (failed: ITaskFailed) => void;
    callback?: (finished: ITask) => void;
    onTaskUpdate?: (batch: IBatchState) => void;
    exporter?: Exporter;
}

/**
 * Conserved batch tasks state
 */
export interface IBatchState {
    queued: ReadonlyArray<ITaskArchived>;
    running: ReadonlyArray<ITaskArchived>;
    done: ReadonlyArray<ITaskArchived>;
    failed: ReadonlyArray<ITaskArchived>;
}

/**
 * Common interface for batch analysis class implementation
 */
export interface IBatchAnalyzer {

    /* Properties */

    readonly queued: ReadonlyArray<ITaskQueued>;
    readonly running: ReadonlyArray<ITaskRunning>;
    readonly done: ReadonlyArray<ITaskArchived>;
    readonly failed: ReadonlyArray<ITaskArchived>;

    /* Methods */

    addTask(url: string, source?: string): Promise<ITaskDone>;

    state(): IBatchState;

    dispose?(): void;

}
