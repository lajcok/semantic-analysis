import axios from 'axios';

export interface IWebPage {
    url: string;
    body: string;
}

export const loadPage = (url: string) =>
    axios.get<string>(url, {responseType: 'text'})
        .then(response => ({
            url,
            body: response.data,
        }));
