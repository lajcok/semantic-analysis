/**
 * Maps all own properties to a function and
 * creates new object accordingly
 * *
 * @param object Input object
 * @param callback Mapper callback function
 */
export function map<T extends object, F>(
    object: T,
    callback: (value: T[keyof T], key: keyof T, self: T) => F,
) {
    const result: any = {};
    for (const key in object) {
        if (object.hasOwnProperty(key)) {
            result[key] = callback(object[key], key, object);
        }
    }
    return result;
}
